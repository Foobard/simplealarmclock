from datetime import datetime
import click
import time
from validator import validate_player, validate_alarm_time
from alarmtools import mins_to_secs, is_alarm_time, launch_alarm


@click.command()
@click.option('--player', callback=validate_player, required=True, metavar='PLAYER', help='Which player to use: mplayer, vlc etc')
@click.option('--time', callback=validate_alarm_time, required=True, metavar='TIME', help='alarm time: HH:MM')
@click.option('--sound', type=click.Path(exists=True, dir_okay=False), required=True, help='file to play')
@click.option('--repeat',  nargs=2, type=int, metavar='MIN TIMES', help='run alarm TIMES times in MIN minutes')
def tick(sleep_time=1, **args):
    """wait for the alarm to go off"""
    while True:
        if is_alarm_time(datetime.now(), args['time']):
            launch_alarm(args['player'], args['sound'])
            break
        else:
            time.sleep(sleep_time)
    if args['repeat']:
        for i in range(args['repeat'][1]):
            time.sleep(mins_to_secs(args['repeat'][0]))
            launch_alarm(args['player'], args['sound'])
