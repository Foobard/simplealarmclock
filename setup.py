from setuptools import setup


setup(
    name='alarm',
    version='1.2',
    py_modules=['aalarm', 'validator', 'alarmtools'],
    install_requires=['click'],
    entry_points="""[console_scripts]
    alarm=aalarm:tick""",
    )
