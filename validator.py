import subprocess
import re
import click


def validate_player(ctx, param, player):
    """check if the player exists"""
    try:
        return subprocess.check_output(['which', player]).strip()
    except subprocess.CalledProcessError:
        raise click.BadParameter('player {} not found'.format(player))


def validate_alarm_time(ctx, param, alarm_time):
    """validate input time format"""
    time_pattern = re.compile('^\d{2}:\d{2}$')
    if not time_pattern.match(alarm_time):
        raise click.BadParameter('wrong time format. Expecting HH:MM')
    hour, minute = map(int, alarm_time.split(':'))
    if not (0 <= hour < 24 and 0 <= minute < 60):
        raise click.BadParameter("'hour' = [0, 24) | 'minute' = [0, 60)")
    else:
        return {'hour': hour, 'minute': minute}
