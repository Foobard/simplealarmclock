import subprocess
import time


def mins_to_secs(minutes):
    """convert minutes to seconds"""
    return minutes * 60


def get_sound_duration(file_path):
    """calculate the input file duration in secs"""
    ffprobe = ["ffprobe", "-v", "error", "-show_entries", "format=duration",
               "-of", "default=noprint_wrappers=1:nokey=1", file_path]
    duration = subprocess.Popen(ffprobe, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return duration.communicate()[0].strip()


def is_alarm_time(time_now, alarm_time):
    """check if it's time to go off"""
    return time_now.hour == alarm_time['hour'] and time_now.minute == alarm_time['minute']


def launch_alarm(player_loc, file_loc):
    """play file with player """
    proc = subprocess.Popen(['{player_loc}'.format(player_loc=player_loc),
                            '{file_loc}'.format(file_loc=file_loc)],
                            stdout=subprocess.PIPE)
    try:
        time.sleep(float(get_sound_duration(file_loc)))
    except TypeError as e:
        print('Error: {error_message}'.format(error_message=e))
    finally:
        proc.terminate()
        proc.wait()
